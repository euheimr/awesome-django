I have come to the decision to shut down all my projects.


I have been a software developer for 33 years and it saddens deeply me to see what has become of the industry that formed the basis of my identity for so long.


Due to many situations (some of which are already public knowledge and others that are not), I don't want to be associated with Python or Django (projects, organizations, and events). I can't in good conscience continue to put by weight and support (economic and technical) behind them. I have also stopped attending Django and Python events as a speaker or attendee. The risks and costs outweight the benefits. Many of them are not even about Python or Django anymore.


I've moved on and software development is no longer my main source of income. As such maintaining so many projects is a drain of my resources and time. I'm shutting down all active projects and closing or restricting the few social media accounts that have not yet censored me. I will only continue to particiapte in Mayan EDMS due to the huge number of people that rely on it and have supported it over the 8 years of its existance. If I ever release new projects in the future I would do so under pseudonyms to protect myself and those who support me.


Thank you very much for the huge support during these past years I wish things would have ended up differently. I have some faith that things will improve in the future as the industry self-corrects.


Best regards,

Roberto Rosario